/**
*
* Retake site notifier for the Splewis plugin 
* By: Somighten
* https://bitbucket.org/somighten-yo/retake-site-notice/src
*
* Prints multipule notifications OnSitePicked
* Version 0.3
*
* TODO: create README with install instructions and convar list
* TODO: add HUD notice
* TODO: add color to the hint notice
*
**/

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <retakes>
#include <timers>

#pragma semicolon 1
#pragma newdecls required


public Plugin myinfo = {
	name = "Retake Site Notice",
	author = "Somighten",
	description = "Display Site Notices",
	version = "0.3",
	url = "https://bitbucket.org/somighten-yo/retake-site-notice/src"
};

/* Convars for chat settings */
ConVar g_chat_enable;
ConVar g_chat_number;
ConVar g_chat_time;
/* Convars for hint settings */
ConVar g_hint_enable;
ConVar g_hint_number;
ConVar g_hint_time;

public void OnPluginStart()
{
	g_chat_enable = CreateConVar("sm_notice_chat_enable", "1", "0/1 Does plugin print to chat");
	g_chat_number = CreateConVar("sm_notice_chat_number", "3", "How many times does chat print");
	g_chat_time = CreateConVar("sm_notice_chat_time", "4", "How many seconds apart are chat notices");

	g_hint_number = CreateConVar("sm_notice_hint_number", "3", "How many times does hint print");
	g_hint_time = CreateConVar("sm_notice_hint_time", "5", "How many seconds apart are hint notices");
	g_hint_enable = CreateConVar("sm_notice_hint_enable", "1", "0/1 Does plugin print hint");	
}

/* Called from Splewis retake */
public void Retakes_OnSitePicked(Bombsite& site)
{
	/* Variable for the timer */
	float cSec = g_chat_time.FloatValue;
	float hSec = g_hint_time.FloatValue;

	/* Only display hint if enabled */
	if(g_hint_enable.IntValue == 1)
	{
		for(int i =0; i < g_hint_number.IntValue; i++)
		{
			/* Print hint message every hint_time */
			CreateTimer(hSec, SiteHint, site);
			hSec += g_hint_time.IntValue;
		}
	}
	
	/* Only dispay chat if enabled */
	if(g_chat_enable.IntValue == 1)
	{
		for(int i = 0; i < g_chat_number.IntValue; i++)
		{
			/* Print to chat every chat_time interval */
			CreateTimer(cSec, SiteChat, site);
			cSec += g_chat_time.IntValue;
		}
	}
}
/* Chat timers */
public Action SiteChat(Handle timer, any site)
{

	if(site == BombsiteA) PrintToChatAll(" \x02Bomb Site\x01 is \x07AA \x04AA \x10AA \x02AA \x06AA");

	if(site == BombsiteB) PrintToChatAll(" \x02Bomb Site\x01 is \x10BB \x08BB \x03BB \x06BB \0x2BB");
}
/* Hint timers */
public Action SiteHint(Handle timer, any site)
{
	if(site == BombsiteA) if(g_hint_enable.IntValue == 1) PrintHintTextToAll("Bomb is on A Site");

	if(site == BombsiteB) if(g_hint_enable.IntValue == 1) PrintHintTextToAll("Bomb is on B Site");
}

